# Jekyll TailwindCSS Template
### A theme-free Jekyll boilerplate preconfigured with TailwindCSS

TailwindCSS is ready to use out of the gate, and is utilizing Tailwinds default Purge settings via PostCSS. If you would like to alter what gets purged on build, you can head over to https://tailwindcss.com/docs/installation/ to follow Tailwinds guide.

After cloning, install the dependencies

`npm install`

To start your local Jekyll server and automatically update your browser on file save

` bundle exec jekyll serve  --livereload`

Once you've made your styling changes and are ready to build, keep in mind Tailwind won't purge unused styles unless your NODE_ENV is set to production when you run the build script

`NODE_ENV=production npm run build:css`

Or if you want to set the NODE_ENV variable by itself

`export NODE_ENV=development`
    
If you want to automatically check for styling changes

`npm run watch`